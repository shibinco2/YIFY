package co.shibin.yify_android.Models.DBModel;

import java.util.Date;

import co.shibin.yify_android.Models.Movie;


public class DataWishlist {
    private String id;
    private String movieId;
    private Movie movie;
    private Date date;

    public void setMovie(Movie movie){
        this.movie = movie;
    }

    public void setMovieId(String movieId){
        this.movieId = movieId;
    }

    public void setDate(){
        this.date = new Date();
    }
}
