package co.shibin.yify_android.Models.DBModel;

import java.util.List;

import co.shibin.yify_android.Models.Movie;


public class DBmodelRoot {
    private List<Movie> data;

    public void setData(List<Movie> data){
        this.data = data;
    }
    public List<Movie> getData(){
        return this.data;
    }
}
